import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;


public class HBaseScrabble {
    private Configuration config;
    private HBaseAdmin hBaseAdmin;

    /**
     * The Constructor. Establishes the connection with HBase.
     * @param zkHost
     * @throws IOException
     */
    public HBaseScrabble(String zkHost) throws IOException {
        config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", zkHost.split(":")[0]);
        config.set("hbase.zookeeper.property.clientPort", zkHost.split(":")[1]);
        HBaseConfiguration.addHbaseResources(config);
        this.hBaseAdmin = new HBaseAdmin(config);
    }

    private byte[] table = Bytes.toBytes("Scrabble1");
    public void createTable() throws IOException {
        byte[] cf = Bytes.toBytes("ScrabbleData");

        HTableDescriptor hTable = new HTableDescriptor(table);
        HColumnDescriptor family = new HColumnDescriptor(cf);
        family.setMaxVersions(10);
        hTable.addFamily(family);

        this.hBaseAdmin.createTable(hTable);
    }

    public void loadTable(String folder)throws IOException{
	    BufferedReader br = null;
	    String line = "";
	    String cvsSplitBy = ",";
	    
        byte[] cf = Bytes.toBytes("ScrabbleData");

        byte[] c1 = Bytes.toBytes("GAMEID");
        byte[] c2 = Bytes.toBytes("TOURNEYID");
        byte[] c3 = Bytes.toBytes("TIE");
        byte[] c4 = Bytes.toBytes("WINNERID");
        byte[] c5 = Bytes.toBytes("WINNERNAME");
        byte[] c6 = Bytes.toBytes("WINNERSCORE");
        byte[] c7 = Bytes.toBytes("WINNEROLDRATING");
        byte[] c8 = Bytes.toBytes("WINNERNEWRATING");
        byte[] c9 = Bytes.toBytes("WINNERPOS");
        byte[] c10 = Bytes.toBytes("LOSERID");
        byte[] c11 = Bytes.toBytes("LOSERNAME");
        byte[] c12 = Bytes.toBytes("LOSERSCORE");
        byte[] c13 = Bytes.toBytes("LOSEROLDRATING");
        byte[] c14 = Bytes.toBytes("LOSERNEWRATING");
        byte[] c15 = Bytes.toBytes("LOSERPOS");
        byte[] c16 = Bytes.toBytes("ROUND");
        byte[] c17 = Bytes.toBytes("DIVISION");
        byte[] c18 = Bytes.toBytes("DATE");
        byte[] c19 = Bytes.toBytes("LEXICON");

        HTable hTable = new HTable(config,table);

	    try {
	        br = new BufferedReader(new FileReader(folder + "/scrabble_games.csv"));
	        
	        line = br.readLine();
            String[] colnames = line.split(cvsSplitBy);
    		//System.out.println("col0=" + colnames[0] + ", col1=" + colnames[1] + ", ");
	        
	        while ((line = br.readLine()) != null) {
	            String[] vals = line.split(cvsSplitBy);
	            //System.out.println("vals0= " + vals[0] + " , vals1=" + vals[1] + ", ");	
	            putGame(hTable, cf, 
	            		c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, 
	            		vals[0],
	            		vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7], vals[8], vals[9], vals[10], vals[11], vals[12], vals[13], vals[14], vals[15], vals[16], vals[17], vals[18]);
	        }
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
            System.out.println("XXX There is no scrabble_games.csv file there. XXX");
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        if (br != null) {
	            try {
	                br.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
    }
    
    private void putGame(HTable hTable, byte[] cf, 
            byte[] c1, byte[] c2, byte[] c3, byte[] c4, byte[] c5, byte[] c6, byte[] c7, byte[] c8, byte[] c9, byte[] c10, byte[] c11, byte[] c12, byte[] c13, byte[] c14, byte[] c15, byte[] c16, byte[] c17, byte[] c18, byte[] c19,
            String game, 
            String c1Val, String c2Val, String c3Val, String c4Val, String c5Val, String c6Val, String c7Val, String c8Val, String c9Val, String c10Val, String c11Val, String c12Val, String c13Val, String c14Val, String c15Val, String c16Val, String c17Val, String c18Val, String c19Val
        ) throws InterruptedIOException, RetriesExhaustedWithDetailsException {

        byte[] key = Bytes.toBytes(game);

        byte[] value_c1 = Bytes.toBytes(c1Val);
        byte[] value_c2 = Bytes.toBytes(c2Val);
        byte[] value_c3 = Bytes.toBytes(c3Val);
        byte[] value_c4 = Bytes.toBytes(c4Val);
        byte[] value_c5 = Bytes.toBytes(c5Val);
        byte[] value_c6 = Bytes.toBytes(c6Val);
        byte[] value_c7 = Bytes.toBytes(c7Val);
        byte[] value_c8 = Bytes.toBytes(c8Val);
        byte[] value_c9 = Bytes.toBytes(c9Val);
        byte[] value_c10 = Bytes.toBytes(c10Val);
        byte[] value_c11 = Bytes.toBytes(c11Val);
        byte[] value_c12 = Bytes.toBytes(c12Val);
        byte[] value_c13 = Bytes.toBytes(c13Val);
        byte[] value_c14 = Bytes.toBytes(c14Val);
        byte[] value_c15 = Bytes.toBytes(c15Val);
        byte[] value_c16 = Bytes.toBytes(c16Val);
        byte[] value_c17 = Bytes.toBytes(c17Val);
        byte[] value_c18 = Bytes.toBytes(c18Val);
        byte[] value_c19 = Bytes.toBytes(c19Val);

        Put put = new Put(key);
        long ts = System.currentTimeMillis();

        put.add(cf, c1, ts, value_c1);
        put.add(cf, c2, ts, value_c2);
        put.add(cf, c3, ts, value_c3);
        put.add(cf, c4, ts, value_c4);
        put.add(cf, c5, ts, value_c5);
        put.add(cf, c6, ts, value_c6);
        put.add(cf, c7, ts, value_c7);
        put.add(cf, c8, ts, value_c8);
        put.add(cf, c9, ts, value_c9);
        put.add(cf, c10, ts, value_c10);
        put.add(cf, c11, ts, value_c11);
        put.add(cf, c12, ts, value_c12);
        put.add(cf, c13, ts, value_c13);
        put.add(cf, c14, ts, value_c14);
        put.add(cf, c15, ts, value_c15);
        put.add(cf, c16, ts, value_c16);
        put.add(cf, c17, ts, value_c17);
        put.add(cf, c18, ts, value_c18);
        put.add(cf, c19, ts, value_c19);

        hTable.put(put);

        System.out.println("Added Game: "+game);
    }
    
    private void deleteTable() throws IOException {
        this.hBaseAdmin.disableTable(table);
        this.hBaseAdmin.deleteTable(table);
    }

    /**
     * This method generates the key
     * @param values The value of each column
     * @param keyTable The position of each value that is required to create the key in the array of values.
     * @return The encoded key to be inserted in HBase
     */
    private byte[] getKey(String[] values, int[] keyTable) {
        String keyString = "";
        for (int keyId : keyTable){
            keyString += values[keyId];
        }
        byte[] key = Bytes.toBytes(keyString);

        return key;
    }



    public List<String> query1(String tourneyid, String winnername) throws IOException {
        byte[] cf = Bytes.toBytes("ScrabbleData");
        HTable hTable = new HTable(config,table);
        Scan scan = new Scan();

        Filter f1 = new SingleColumnValueFilter(
            cf, 
            Bytes.toBytes("TOURNEYID"), 
            CompareFilter.CompareOp.EQUAL, Bytes.toBytes(tourneyid)
        );
        Filter f2 = new SingleColumnValueFilter(
            cf, 
            Bytes.toBytes("WINNERNAME"), 
            CompareFilter.CompareOp.EQUAL, Bytes.toBytes(winnername)
        );

        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        filterList.addFilter(f1);
        filterList.addFilter(f2);

        scan.setFilter(filterList);

        ResultScanner rs = hTable.getScanner(scan);
        Result result = rs.next();
        List<String> stringList = new ArrayList<String>();

        while (result!=null && !result.isEmpty()){
            String key = Bytes.toString(result.getRow());
            String gid = Bytes.toString(result.getValue(cf,Bytes.toBytes("GAMEID")));
            String lname = Bytes.toString(result.getValue(cf,Bytes.toBytes("LOSERNAME")));
            String lid = Bytes.toString(result.getValue(cf,Bytes.toBytes("LOSERID")));
            //System.out.println("Key: "+key + " GAMEID: "+gid +" LOSERNAME: "+lname +" LOSERID: "+lid);

            stringList.add(lid);

            result = rs.next();
        }

        return stringList;
    }

    public List<String> query2(String firsttourneyid, String lasttourneyid) throws IOException {
        byte[] cf = Bytes.toBytes("ScrabbleData");
        HTable hTable = new HTable(config,table);
        Scan scan = new Scan();

        Filter f1 = new SingleColumnValueFilter(
            cf, 
            Bytes.toBytes("TOURNEYID"), 
            CompareFilter.CompareOp.GREATER_OR_EQUAL, Bytes.toBytes(firsttourneyid)
        );
        Filter f2 = new SingleColumnValueFilter(
            cf, 
            Bytes.toBytes("TOURNEYID"), 
            CompareFilter.CompareOp.LESS_OR_EQUAL, Bytes.toBytes(lasttourneyid)
        );

        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        filterList.addFilter(f1);
        filterList.addFilter(f2);

        scan.setFilter(filterList);

        ResultScanner rs = hTable.getScanner(scan);
        Result result = rs.next();
        List<String> stringList = new ArrayList<String>();
        HashMap<String, Integer> hashmap = new HashMap<String, Integer>();

        while (result!=null && !result.isEmpty()){
            String key = Bytes.toString(result.getRow());
            String gid = Bytes.toString(result.getValue(cf,Bytes.toBytes("GAMEID")));
            String lname = Bytes.toString(result.getValue(cf,Bytes.toBytes("LOSERNAME")));
            String lid = Bytes.toString(result.getValue(cf,Bytes.toBytes("LOSERID")));
            String wid = Bytes.toString(result.getValue(cf,Bytes.toBytes("WINNERID")));
            String tid = Bytes.toString(result.getValue(cf,Bytes.toBytes("TOURNEYID")));
            //System.out.println("x GAMEID: "+gid + " WINNERID: "+wid +" LOSERID: "+lid +" TOURNEYID: "+tid);

            String wlpair =  wid + "-" + lid;

            if (hashmap.containsKey(wlpair)) {
                hashmap.put(wlpair, hashmap.get(wlpair) + 1);
            } else {
                hashmap.put(wlpair, 1);
            }

            result = rs.next();
        }

        for (HashMap.Entry<String, Integer> entry : hashmap.entrySet())
        {
            String wlpair = entry.getKey();
            Integer c = entry.getValue();
            if (c > 1) {
                stringList.add("w-l:" + wlpair);
            }
            //System.out.println(entry.getKey() + "/" + entry.getValue());
        }

        return stringList;
    }

    public List<String> query3(String tourneyid) throws IOException {
        byte[] cf = Bytes.toBytes("ScrabbleData");
        HTable hTable = new HTable(config,table);

        Scan scan = new Scan();

        Filter f1 = new SingleColumnValueFilter(
            cf, 
            Bytes.toBytes("TOURNEYID"), 
            CompareFilter.CompareOp.EQUAL, Bytes.toBytes(tourneyid)
        );
        Filter f2 = new SingleColumnValueFilter(
            cf, 
            Bytes.toBytes("TIE"), 
            CompareFilter.CompareOp.EQUAL, Bytes.toBytes("True")
        );

        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        filterList.addFilter(f1);
        filterList.addFilter(f2);

        scan.setFilter(filterList);

        ResultScanner rs = hTable.getScanner(scan);
        Result result = rs.next();
        List<String> stringList = new ArrayList<String>();

        while (result!=null && !result.isEmpty()){
            String key = Bytes.toString(result.getRow());
            String gid = Bytes.toString(result.getValue(cf,Bytes.toBytes("GAMEID")));
            String lname = Bytes.toString(result.getValue(cf,Bytes.toBytes("LOSERNAME")));
            String lid = Bytes.toString(result.getValue(cf,Bytes.toBytes("LOSERID")));
            //System.out.println("Key: "+key + " GAMEID: "+gid +" LOSERNAME: "+lname +" LOSERID: "+lid);

            stringList.add(gid);

            result = rs.next();
        }

        return stringList;
    }


    public static void main(String[] args) throws IOException {
        if(args.length<2){
            System.out.println("Error: \n1)ZK_HOST:ZK_PORT, \n2)action [createTable, loadTable, query1, query2, query3], \n3)Extra parameters for loadTables and queries:\n" +
                    "\ta) If loadTable: csvsFolder.\n " +
                    "\tb) If query1: tourneyid winnername.\n  " +
                    "\tc) If query2: firsttourneyid lasttourneyid.\n  " +
                    "\td) If query3: tourneyid.\n  ");
            System.exit(-1);
        }
        HBaseScrabble hBaseScrabble = new HBaseScrabble(args[0]);
        if(args[1].toUpperCase().equals("CREATETABLE")){
            hBaseScrabble.createTable();
        }
        else if(args[1].toUpperCase().equals("DELETETABLE")){
        	hBaseScrabble.deleteTable();
        }
        else if(args[1].toUpperCase().equals("LOADTABLE")){
            if(args.length!=3){
                System.out.println("Error: 1) ZK_HOST:ZK_PORT, 2)action [createTables, loadTables], 3)csvsFolder");
                System.exit(-1);
            }
            else if(!(new File(args[2])).isDirectory()){
                System.out.println("Error: Folder "+args[2]+" does not exist.");
                System.exit(-2);
            }
            hBaseScrabble.loadTable(args[2]);
        }
        else if(args[1].toUpperCase().equals("QUERY1")){
            if(args.length!=4){
                System.out.println("Error: 1) ZK_HOST:ZK_PORT, 2)query1, " +
                        "3) tourneyid 4) winnername");
                System.exit(-1);
            }

            List<String> opponentsName = hBaseScrabble.query1(args[2], args[3]);
            System.out.println("There are "+opponentsName.size()+" opponents of winner "+args[3]+" that play in tourney "+args[2]+".");
            System.out.println("The list of opponents is: "+Arrays.toString(opponentsName.toArray(new String[opponentsName.size()])));
        }
        else if(args[1].toUpperCase().equals("QUERY2")){
            if(args.length!=4){
                System.out.println("Error: 1) ZK_HOST:ZK_PORT, 2)query2, " +
                        "3) firsttourneyid 4) lasttourneyid");
                System.exit(-1);
            }
            List<String> playerNames =hBaseScrabble.query2(args[2], args[3]);
            System.out.println("There are "+playerNames.size()+" players that participates in more than one tourney between tourneyid "+args[2]+" and tourneyid "+args[3]+" .");
            System.out.println("The list of players is: "+Arrays.toString(playerNames.toArray(new String[playerNames.size()])));
        }
        else if(args[1].toUpperCase().equals("QUERY3")){
            if(args.length!=3){
                System.out.println("Error: 1) ZK_HOST:ZK_PORT, 2) query3, " +
                        "3) tourneyid");
                System.exit(-1);
            }
            List<String> games = hBaseScrabble.query3(args[2]);
            System.out.println("There are "+games.size()+" that ends in tie in tourneyid "+args[2]+" .");
            System.out.println("The list of games is: "+Arrays.toString(games.toArray(new String[games.size()])));
        }
        else{
            System.out.println("Error: \n1)ZK_HOST:ZK_PORT, \n2)action [createTable, loadTable, query1, query2, query3], \n3)Extra parameters for loadTables and queries:\n" +
                    "\ta) If loadTable: csvsFolder.\n " +
                    "\tb) If query1: tourneyid winnername.\n  " +
                    "\tc) If query2: firsttourneyid lasttourneyid.\n  " +
                    "\td) If query3: tourneyid.\n  ");
            System.exit(-1);
        }

    }



}

